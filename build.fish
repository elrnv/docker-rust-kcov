#!/usr/bin/env fish

set rust_version 1.42.0
set cur_dir $PWD

git clone https://github.com/SimonKagstrom/kcov
cd kcov

wget https://raw.githubusercontent.com/rust-lang/docker-rust/master/$rust_version/buster/Dockerfile &&\
cat Dockerfile.1 >> Dockerfile &&\
docker build --tag elrnv/rust-kcov:rust-$rust_version . &&\
docker push elrnv/rust-kcov:rust-$rust_version

cd $cur_dir


